"use strict";

var cssClass = '<article class="pizzaThumbnail">';
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
data.sort(function (a, b) {
  var price_small = a.price_small,
      price_large = a.price_large;
  if (price_small - b.price_small == 0) return price_large - b.price_large;
  return price_small - b.price_small;
});

function tomatoBase(element) {
  return element.base == 'tomate';
}

var total = [''];
data.filter(tomatoBase).forEach(function (pizza) {
  var name = pizza.name,
      image = pizza.image,
      price_small = pizza.price_small,
      price_large = pizza.price_large;
  var href = '<a href="' + image + '"><img src="' + image + '"/>';
  var section = '<section><h4>' + name + '</h4>';
  var list = '<ul><li>Prix petit format : ' + price_small + '€</li><li>Prix grand format : ' + price_large + '€</li></ul>';
  var html = cssClass + href + section + list + '</section></a></article>';
  total.push(html);
});
document.querySelector('.pageContainer').innerHTML = total;